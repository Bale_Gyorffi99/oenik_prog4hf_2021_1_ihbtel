﻿// <copyright file="GPULogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GPUshop_logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GPU_Models;
    using GPUshop_repository;

    /// <summary>
    /// Class of GPU. Implements IGPULogic interface. Contains methods that relies on the same repository class as the controller one.
    /// </summary>
    public class GPULogic : IGPULogic
    {
        private readonly IGPURepository gpuRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="GPULogic"/> class.
        /// </summary>
        /// <param name="ogRepo"> IGPURepository object. </param>
        public GPULogic(IGPURepository ogRepo)
        {
            this.gpuRepo = ogRepo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GPULogic"/> class.
        /// </summary>
        public GPULogic()
        {
            this.gpuRepo = new GPURepository();
        }

        /// <summary>
        /// Adds an entity to the table.
        /// </summary>
        /// <param name="obj"> PSU object. </param>
        public void CreateItem(GPU obj)
        {
            this.gpuRepo.CreateItem(obj);
        }

        /// <summary>
        /// Deletes entity by given id.
        /// </summary>
        /// <param name="id"> Id of the deletable entity. </param>
        public void DeleteItem(int id)
        {
            this.gpuRepo.DeleteItem(id);
        }

        /// <summary>
        /// Returns a List of all PSU in the table.
        /// </summary>
        /// <returns> List of all PSU. </returns>
        public IQueryable<GPU> GetAll()
        {
            return this.gpuRepo.GetAll();
        }

        /// <summary>
        /// Returns specified entity.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <returns> specified entity. </returns>
        public GPU GetOne(int id)
        {
            return this.gpuRepo.GetOne(id);
        }

        /// <summary>
        /// Updates the specified entity's name.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <param name="newValue"> new name. </param>
        public void UpdateName(int id, string newValue)
        {
            this.gpuRepo.UpdateName(id, newValue);
        }

        /// <summary>
        /// Updates the specified entity's price.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <param name="newValue"> new price. </param>
        public void UpdatePrice(int id, int newValue)
        {
            this.gpuRepo.UpdatePrice(id, newValue);
        }

        /// <summary>
        /// Updates the specified entity's price.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <param name="newValue"> false / true -> not VR ready or VR ready. </param>
        public void UpdateVR(int id, bool newValue)
        {
            this.gpuRepo.UpdateVR(id, newValue);
        }

        /// <summary>
        /// Averages the actual table all entity's price.
        /// </summary>
        /// <returns> Average price. </returns>
        public decimal AVGPrice()
        {
            decimal sumPrice = 0;
            foreach (var item in this.gpuRepo.GetAll())
            {
                sumPrice += item.PRICE;
            }

            sumPrice /= this.gpuRepo.GetAll().Count();
            return sumPrice;
        }

        /// <summary>
        /// Filters gpu entities in 2 price range.
        /// </summary>
        /// <param name="gpuMinPrice"> lowest price. </param>
        /// <param name="gpuMaxPrice"> highest price. </param>
        /// <returns> filtered List. </returns>
        public ICollection<GPU> FilterPriceBetween(int gpuMinPrice, int gpuMaxPrice)
        {
            List<GPU> filtered = new List<GPU>();
            foreach (var item in this.gpuRepo.GetAll())
            {
                if (item.PRICE > gpuMinPrice)
                {
                    if (item.PRICE < gpuMaxPrice)
                    {
                        filtered.Add(item);
                    }
                }
            }

            return filtered;
        }

        /// <summary>
        /// Looks for the last entity's ID.
        /// </summary>
        /// <returns> last entity's ID. </returns>
        public decimal GetMaxIdx()
        {
            return this.gpuRepo.GetAll().Last().GPU_ID;
        }
    }
}
