﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GPUshop_logic
{
    using System;
    using System.Linq;

    /// <summary>
    /// All table's must interface. contains CRUD methods.
    /// </summary>
    /// <typeparam name="T">Every taable.</typeparam>
    public interface ILogic<T>
            where T : class
    {
        /// <summary>
        /// Adds an entity to the table.
        /// </summary>
        /// <param name="obj"> T object. </param>
        void CreateItem(T obj);

        /// <summary>
        /// Returns specified entity.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <returns> T entity. </returns>
        T GetOne(int id);

        /// <summary>
        /// Returns a List of all entities in the table.
        /// </summary>
        /// <returns> List of entities. </returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Updates the specified entity's name.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <param name="newValue"> new name. </param>
        void UpdateName(int id, string newValue);

        /// <summary>
        /// Updates the specified entity's price.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <param name="newValue"> new price. </param>
        void UpdatePrice(int id, int newValue);

        /// <summary>
        /// Deletes entity by given id.
        /// </summary>
        /// <param name="id"> Id of the deletable entity. </param>
        void DeleteItem(int id);

        /// <summary>
        /// Looks for the last entity's ID.
        /// </summary>
        /// <returns> last entity's ID. </returns>
        decimal GetMaxIdx();
    }
}
