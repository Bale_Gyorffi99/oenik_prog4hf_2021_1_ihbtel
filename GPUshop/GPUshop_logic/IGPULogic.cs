﻿// <copyright file="IGPULogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GPUshop_logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GPU_Models;

    /// <summary>
    /// Interface of GPU class. Contains specific methods.
    /// </summary>
    public interface IGPULogic : ILogic<GPU>
    {
        /// <summary>
        /// Updates the specified entity's price.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <param name="newValue"> bool value -> not VR ready or VR ready. </param>
        void UpdateVR(int id, bool newValue);

        // TODO: Avg price / Filter here!
    }
}
