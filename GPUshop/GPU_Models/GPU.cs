﻿// <copyright file="GPU.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GPU_Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// GPU table.
    /// </summary>
    [Table("GPU")]
    public class GPU : ObservableObject
    {
        /// <summary>
        /// Gets or sets ID of GPU.
        /// </summary>
        [Key]
        public int GPU_ID { get; set; }

        /// <summary>
        /// Gets or sets serialnumber of GPU.
        /// </summary>
        public int SERIALNUMBER { get; set; }

        /// <summary>
        /// Gets or sets Name of GPU.
        /// </summary>
        public string GPU_NAME { get; set; }

        /// <summary>
        /// Gets or sets the price of GPU.
        /// </summary>
        public int PRICE { get; set; }

        /// <summary>
        /// Gets or sets the ram size of GPU.
        /// </summary>
        public int RAM_SIZE { get; set; }

        /// <summary>
        /// Gets or sets the clock speed of GPU.
        /// </summary>
        public int CLOCK_SPEED { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether gets or sets the chance of being VR ready of GPU.
        /// </summary>
        public bool VR_READY { get; set; }

        /// <summary>
        /// Copies from other GPU.
        /// </summary>
        /// <param name="other"> other gpu. </param>
        public void CopyFrom(GPU other)
        {
            // WARNING: creates shallow copy - no problem for now
            // WARNING: reflection is SLOOOOOOOOW - no problem for now
            // Possible: using AutoMapper;
            this.GetType().GetProperties().ToList().
                ForEach(property => property.SetValue(this, property.GetValue(other)));
        }
    }
}
