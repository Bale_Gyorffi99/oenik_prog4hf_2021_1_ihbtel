﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Naming", "CA1707:Identifiers should not contain underscores", Justification = " True ", Scope = "member", Target = "~P:Models.GPU.CLOCK_SPEED")]
[assembly: SuppressMessage("Naming", "CA1707:Identifiers should not contain underscores", Justification = " True ", Scope = "member", Target = "~P:Models.GPU.GPU_ID")]
[assembly: SuppressMessage("Naming", "CA1707:Identifiers should not contain underscores", Justification = " True ", Scope = "member", Target = "~P:Models.GPU.GPU_NAME")]
[assembly: SuppressMessage("Naming", "CA1707:Identifiers should not contain underscores", Justification = " True ", Scope = "member", Target = "~P:Models.GPU.RAM_SIZE")]
[assembly: SuppressMessage("Naming", "CA1707:Identifiers should not contain underscores", Justification = " True ", Scope = "member", Target = "~P:Models.GPU.VR_READY")]
