﻿// <copyright file="CCgpu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GPUShop_ConsoleClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Gpu class.
    /// </summary>
    public class CCgpu
    {
        /// <summary>
        /// Gets or sets ID of GPU.
        /// </summary>
        public int GPU_ID { get; set; }

        /// <summary>
        /// Gets or sets serialnumber of GPU.
        /// </summary>
        public int SERIALNUMBER { get; set; }

        /// <summary>
        /// Gets or sets Name of GPU.
        /// </summary>
        public string GPU_NAME { get; set; }

        /// <summary>
        /// Gets or sets the price of GPU.
        /// </summary>
        public int PRICE { get; set; }

        /// <summary>
        /// Gets or sets the ram size of GPU.
        /// </summary>
        public int RAM_SIZE { get; set; }

        /// <summary>
        /// To String method.
        /// </summary>
        /// <returns> string line. </returns>
        public override string ToString()
        {
            return $"ID={this.GPU_ID}/tName={this.GPU_NAME}/tPrice={this.PRICE}/tRam Size={this.RAM_SIZE}";
        }
    }
}
