﻿// <copyright file="GpuModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GPU_CRUDAPP.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;
    using GPU_Models;

    /// <summary>
    /// Model class of GPU.
    /// </summary>
    public class GpuModel : ObservableObject
    {
        private string gpuName;
        private int price;
        private int ramSize;
        private bool vrReady;
        private int clockSpeed;

        /// <summary>
        /// Gets or sets the ID of GPU.
        /// </summary>
        public int GPU_ID { get; set; }

        /// <summary>
        /// Gets or sets name of gpu.
        /// </summary>
        public string GPU_NAME
        {
            get { return this.gpuName; }
            set { this.Set(ref this.gpuName, value); }
        }

        /// <summary>
        /// Gets or sets price.
        /// </summary>
        public int PRICE
        {
            get { return this.price; }
            set { this.Set(ref this.price, value); }
        }

        /// <summary>
        /// Gets or sets ram size.
        /// </summary>
        public int RAM_SIZE
        {
            get { return this.ramSize; }
            set { this.Set(ref this.ramSize, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether VR is ready.
        /// </summary>
        public bool VR_READY
        {
            get { return this.vrReady; }
            set { this.Set(ref this.vrReady, value); }
        }

        /// <summary>
        /// Gets or sets clock speed.
        /// </summary>
        public int CLOCK_SPEED
        {
            get { return this.clockSpeed; }
            set { this.Set(ref this.clockSpeed, value); }
        }

        /// <summary>
        /// creates a copy.
        /// </summary>
        /// <param name="other"> gpu that the copy creates from. </param>
        public void CopyFrom(GpuModel other)
        {
            // WARNING: creates shallow copy - no problem for now
            // WARNING: reflection is SLOOOOOOOOW - no problem for now
            // Possible: using AutoMapper;
            this.GetType().GetProperties().ToList().
                ForEach(property => property.SetValue(this, property.GetValue(other)));
        }
    }
}
