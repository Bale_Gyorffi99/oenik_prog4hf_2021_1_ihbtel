﻿// <copyright file="MainViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GPU_CRUDAPP.VM
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using GPU_CRUDAPP.BL;
    using GPU_CRUDAPP.Data;
    using GPU_Models;

    /// <summary>
    /// Main view model.
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private GpuModel graphicsCardSelected;

        private IGPUlogic logic;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
               : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IGPUlogic>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        /// <param name="logic"> gpu logic. </param>
        internal MainViewModel(IGPUlogic logic)
        {
            this.logic = logic;

            this.All = new ObservableCollection<GpuModel>(this.logic.GetAllGPUs());

            this.AddCmd = new RelayCommand(() => this.logic.AddGPU(this.All));

            this.ModCmd = new RelayCommand(() => this.logic.ModGPU(this.GraphicsCardSelected));

            this.DelCmd = new RelayCommand(() => this.logic.DeleteGpu(this.All, this.GraphicsCardSelected));
        }

        /// <summary>
        /// Gets or Sets the selected Card.
        /// </summary>
        public GpuModel GraphicsCardSelected
        {
            get { return this.graphicsCardSelected; }
            set { this.Set(ref this.graphicsCardSelected, value); }
        }

        /// <summary>
        /// Gets every item from db.
        /// </summary>
        public ObservableCollection<GpuModel> All { get; }

        /// <summary>
        /// Gets add function.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets update function.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets delete function.
        /// </summary>
        public ICommand DelCmd { get; private set; }
    }
}
