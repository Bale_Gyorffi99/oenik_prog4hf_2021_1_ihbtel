﻿// <copyright file="EditorViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GPU_CRUDAPP.VM
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using GalaSoft.MvvmLight;
    using GPU_CRUDAPP.Data;

    /// <summary>
    /// VM of Editor window.
    /// </summary>
    public class EditorViewModel : ViewModelBase
    {
        private GpuModel gpuModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorViewModel"/> class.
        /// </summary>
        public EditorViewModel()
        {
            this.GpuModel = new GpuModel();
        }

        /// <summary>
        /// Gets or sets GpuModel.
        /// </summary>
        public GpuModel GpuModel
        {
            get { return this.gpuModel; }
            set { this.Set(ref this.gpuModel, value); }
        }
    }
}
