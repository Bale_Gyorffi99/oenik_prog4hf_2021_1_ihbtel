﻿// <copyright file="IEditorService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GPU_CRUDAPP.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GPU_CRUDAPP.Data;

    /// <summary>
    /// Interface of Editor window.
    /// </summary>
    public interface IEditorService
    {
        /// <summary>
        /// Edits a GPU.
        /// </summary>
        /// <param name="g"> gpu object. </param>
        /// <returns> edited or not. </returns>
        bool EditGPU(GpuModel g);
    }
}
