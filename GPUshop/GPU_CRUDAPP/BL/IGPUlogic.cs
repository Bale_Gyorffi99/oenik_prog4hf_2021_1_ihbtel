﻿// <copyright file="IGPUlogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GPU_CRUDAPP.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GPU_CRUDAPP.Data;

    /// <summary>
    /// Gpu logic of wpf.
    /// </summary>
    public interface IGPUlogic
    {
        /// <summary>
        /// Adds a gpu and calls db operation.
        /// </summary>
        /// <param name="list"> where it adds gpu. </param>
        void AddGPU(IList<GpuModel> list);

        /// <summary>
        /// modifies gpu object.
        /// </summary>
        /// <param name="gpuToModify"> Which one it modifies. </param>
        void ModGPU(GpuModel gpuToModify);

        /// <summary>
        /// Deletes gpu and calls db operation.
        /// </summary>
        /// <param name="list"> from where it deletes. </param>
        /// <param name="gpu"> which one it deletes. </param>
        void DeleteGpu(IList<GpuModel> list, GpuModel gpu);

        /// <summary>
        /// Gets all gpu model.
        /// </summary>
        /// <returns> returns all gpu model. </returns>
        IList<GpuModel> GetAllGPUs();
    }
}
