﻿// <copyright file="GPUlogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GPU_CRUDAPP.BL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;
    using GPU_CRUDAPP.Data;
    using GPU_CRUDAPP.VM;
    using GPU_Models;
    using GPUshop_logic;

    /// <summary>
    /// class of GPU logic.
    /// </summary>
    public class GPUlogic : IGPUlogic
    {
        private static readonly Random R = new Random();
        private readonly IEditorService editorService;
        private readonly IMessenger messengerService;
        private readonly Factory fc;

        /// <summary>
        /// Initializes a new instance of the <see cref="GPUlogic"/> class.
        /// </summary>
        /// <param name="service"> service.</param>
        /// <param name="messenger"> messenger. </param>
        /// <param name="factory"> factory. </param>
        public GPUlogic(IEditorService service, IMessenger messenger, Factory factory)
        {
            this.editorService = service;
            this.messengerService = messenger;
            this.fc = factory;

            // this.logic = logic;
        }

        /// <summary>
        /// Adds a gpu and calls db operation.
        /// </summary>
        /// <param name="list"> where it adds gpu. </param>
        public void AddGPU(IList<GpuModel> list)
        {
            GpuModel newGpu = new GpuModel();
            if (this.editorService.EditGPU(newGpu) == true)
            {
                newGpu.GPU_ID = list.Last().GPU_ID + 1;
                newGpu.CLOCK_SPEED = R.Next(1600, 2100);
                newGpu.RAM_SIZE = R.Next(4, 16);
                GPU card = new GPU()
                {
                    // GPU_ID = newGpu.GPU_ID,
                    CLOCK_SPEED = newGpu.CLOCK_SPEED,
                    PRICE = newGpu.PRICE,
                    GPU_NAME = newGpu.GPU_NAME,
                    RAM_SIZE = newGpu.RAM_SIZE,
                    VR_READY = newGpu.VR_READY,
                };
                list.Add(newGpu);
                this.fc.GetGPULogic.CreateItem(card);
                this.messengerService.Send("ADD OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("ADD CANCEL", "LogicResult");
            }
        }

        /// <summary>
        /// Deletes gpu and calls db operation.
        /// </summary>
        /// <param name="list"> from where it deletes. </param>
        /// <param name="gpu"> which one it deletes. </param>
        public void DeleteGpu(IList<GpuModel> list, GpuModel gpu)
        {
            if (gpu != null && list.Remove(gpu))
            {
                this.fc.GetGPULogic.DeleteItem(gpu.GPU_ID);
                this.messengerService.Send("DELETE OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("DELETE FAILED", "LogicResult");
            }
        }

        /// <summary>
        /// Gets all gpu model.
        /// </summary>
        /// <returns> returns all gpu model. </returns>
        public IList<GpuModel> GetAllGPUs()
        {
            BindingList<GpuModel> gpuBindList = new BindingList<GpuModel>();
            IQueryable<GPU> gPUs = this.fc.GetGPULogic.GetAll();
            foreach (GPU item in gPUs)
            {
                GpuModel newgpu = new GpuModel()
                {
                    GPU_ID = item.GPU_ID,
                    CLOCK_SPEED = item.CLOCK_SPEED,
                    PRICE = item.PRICE,
                    GPU_NAME = item.GPU_NAME,
                    RAM_SIZE = item.RAM_SIZE,
                    VR_READY = item.VR_READY,
                };
                gpuBindList.Add(newgpu);
            }

            return gpuBindList;
        }

        /// <summary>
        /// modifies gpu object.
        /// </summary>
        /// <param name="gpuToModify"> Which one it modifies. </param>
        public void ModGPU(GpuModel gpuToModify)
        {
            if (gpuToModify == null)
            {
                this.messengerService.Send("EDIT FAILED", "LogicResult");
                return;
            }

            GpuModel clone = new GpuModel();
            clone.CopyFrom(gpuToModify);
            if (this.editorService.EditGPU(clone) == true)
            {
                if (clone.GPU_NAME != gpuToModify.GPU_NAME)
                {
                   this.fc.GetGPULogic.UpdateName(gpuToModify.GPU_ID, clone.GPU_NAME);
                }

                if (clone.PRICE != gpuToModify.PRICE)
                {
                   this.fc.GetGPULogic.UpdatePrice(gpuToModify.GPU_ID, clone.PRICE);
                }

                if (clone.VR_READY != gpuToModify.VR_READY)
                {
                   this.fc.GetGPULogic.UpdateVR(gpuToModify.GPU_ID, clone.VR_READY);
                }

                gpuToModify.CopyFrom(clone);
                this.messengerService.Send("EDIT OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("EDIT CANCEL", "LogicResult");
            }
        }
    }
}
