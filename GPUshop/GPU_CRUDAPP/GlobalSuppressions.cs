﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Style", "IDE0044:Add readonly modifier", Justification = "True", Scope = "member", Target = "~F:GPU_CRUDAPP.VM.MainViewModel.graphicsCardSelected")]
[assembly: SuppressMessage("Style", "IDE0044:Add readonly modifier", Justification = "True", Scope = "member", Target = "~F:GPU_CRUDAPP.VM.MainViewModel.logic")]
[assembly: SuppressMessage("Naming", "CA1707:Identifiers should not contain underscores", Justification = "True", Scope = "namespace", Target = "~N:GPU_CRUDAPP.VM")]
[assembly: SuppressMessage("Naming", "CA1707:Identifiers should not contain underscores", Justification = "True", Scope = "namespace", Target = "~N:GPU_CRUDAPP.BL")]
[assembly: SuppressMessage("Naming", "CA1707:Identifiers should not contain underscores", Justification = "True", Scope = "namespace", Target = "~N:GPU_CRUDAPP.Data")]
[assembly: SuppressMessage("Naming", "CA1707:Identifiers should not contain underscores", Justification = "True", Scope = "namespace", Target = "~N:GPU_CRUDAPP.UI")]
[assembly: SuppressMessage("Naming", "CA1707:Identifiers should not contain underscores", Justification = "True", Scope = "namespace", Target = "~N:GPU_CRUDAPP")]
[assembly: SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "True", Scope = "member", Target = "~M:GPU_CRUDAPP.App.#ctor")]
[assembly: SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "True", Scope = "member", Target = "~M:GPU_CRUDAPP.VM.Factory.#ctor")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "True", Scope = "member", Target = "~M:GPU_CRUDAPP.BL.GPUlogic.AddGPU(System.Collections.Generic.IList{GPU_CRUDAPP.Data.GpuModel})")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "True", Scope = "member", Target = "~M:GPU_CRUDAPP.BL.GPUlogic.DeleteGpu(System.Collections.Generic.IList{GPU_CRUDAPP.Data.GpuModel},GPU_CRUDAPP.Data.GpuModel)")]
[assembly: SuppressMessage("Security", "CA5394:Do not use insecure randomness", Justification = "True", Scope = "member", Target = "~M:GPU_CRUDAPP.BL.GPUlogic.AddGPU(System.Collections.Generic.IList{GPU_CRUDAPP.Data.GpuModel})")]
