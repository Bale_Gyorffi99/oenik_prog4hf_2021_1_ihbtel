﻿// <copyright file="App.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GPU_CRUDAPP
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight.Ioc;
    using GalaSoft.MvvmLight.Messaging;
    using GPU_CRUDAPP.BL;
    using GPU_CRUDAPP.UI;
    using GPU_CRUDAPP.VM;
    using GPUshop_data;
    using GPUshop_logic;
    using Microsoft.EntityFrameworkCore;

/// <summary>
/// Interaction logic for App.xaml.
/// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
            ServiceLocator.SetLocatorProvider(() => MyIOC.Instance);

            MyIOC.Instance.Register<IEditorService, EditorServiceThroughWindow>();
            MyIOC.Instance.Register<IMessenger>(() => Messenger.Default);
            MyIOC.Instance.Register<IGPUlogic, GPUlogic>();
            MyIOC.Instance.Register<Factory, Factory>();

            // GPUshopDataBase ctx = new GPUshopDataBase();

            // MyIOC.Instance.Register<DbContext>(() => ctx);
        }

        private class MyIOC : SimpleIoc, IServiceLocator
        {
            public static MyIOC Instance { get; private set; } = new MyIOC();
        }
    }
}
