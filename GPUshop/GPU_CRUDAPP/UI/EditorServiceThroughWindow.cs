﻿// <copyright file="EditorServiceThroughWindow.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GPU_CRUDAPP.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GPU_CRUDAPP.BL;
    using GPU_CRUDAPP.Data;
    using GPU_Models;

    /// <summary>
    /// Editor window call.
    /// </summary>
    public class EditorServiceThroughWindow : IEditorService
    {
        /// <summary>
        /// Edits a gpu model.
        /// </summary>
        /// <param name="g"> a wpf type of gpu. </param>
        /// <returns>return if changed or not. </returns>
        public bool EditGPU(GpuModel g)
        {
            EditorWindow win = new EditorWindow(g);
            return win.ShowDialog() ?? false;
        }
    }
}
