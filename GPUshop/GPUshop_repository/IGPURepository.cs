﻿// <copyright file="IGPURepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GPUshop_repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GPU_Models;

    /// <summary>
    /// Interface of GPU class. Contains specific methods.
    /// </summary>
    public interface IGPURepository : IRepository<GPU>
    {
        /// <summary>
        /// Modifies the specified entity's VR capability.
        /// </summary>
        /// <param name="id"> specified entity's ID. </param>
        /// <param name="newValue"> able to used with VR, or not. </param>
        void UpdateVR(int id, bool newValue);
    }
}
