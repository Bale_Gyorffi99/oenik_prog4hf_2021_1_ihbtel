﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Naming", "CA1707:Identifiers should not contain underscores", Justification = " True ", Scope = "namespace", Target = "~N:GPUshop_repository")]
[assembly: SuppressMessage("Design", "CA1001:Types that own disposable fields should be disposable", Justification = " True ", Scope = "type", Target = "~T:GPUshop_repository.GPURepository")]
[assembly: SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = " True ", Scope = "member", Target = "~M:GPUshop_repository.GPURepository.GetOne(System.Int32)~GPU_Models.GPU")]
