﻿// <copyright file="GPURepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GPUshop_repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GPU_Models;
    using GPUshop_data;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Repository of GPU.
    /// </summary>
    public class GPURepository : IGPURepository
    {
        private readonly GPUshopDataBase dbContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="GPURepository"/> class.
        /// </summary>
        /// <param name="dbContext"> database. </param>
        public GPURepository(GPUshopDataBase dbContext)
        {
            this.dbContext = dbContext;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GPURepository"/> class.
        /// </summary>
        public GPURepository()
        {
            this.dbContext = new GPUshopDataBase();
        }

        /// <summary>
        /// Creates item.
        /// </summary>
        /// <param name="obj"> gpu. </param>
        public void CreateItem(GPU obj)
        {
            this.dbContext.Set<GPU>().Add(obj);
            this.dbContext.SaveChanges();
        }

        /// <summary>
        /// Deletes item.
        /// </summary>
        /// <param name="id"> ID of gpu. </param>
        public void DeleteItem(int id)
        {
            try
            {
                GPU gg = this.dbContext.GPUs.First(x => x.GPU_ID == id);
                this.dbContext.Remove(gg);
            }
            catch (InvalidOperationException)
            {
            }

            this.dbContext.SaveChanges();
        }

        /// <summary>
        /// Gets all GPU.
        /// </summary>
        /// <returns> every gpu. </returns>
        public IQueryable<GPU> GetAll()
        {
            return this.dbContext.Set<GPU>();
        }

        /// <summary>
        /// gets one gpu.
        /// </summary>
        /// <param name="id">ID of gpu.</param>
        /// <returns> gpu object. </returns>
        public GPU GetOne(int id)
        {
            GPU q;
            try
            {
                q = this.dbContext.GPUs.First(x => x.GPU_ID == id);
            }
            catch (Exception)
            {
                q = null;
            }

            return q;
        }

        /// <summary>
        /// Updates name.
        /// </summary>
        /// <param name="id"> ID of gpu. </param>
        /// <param name="newValue"> new name of gpu. </param>
        public void UpdateName(int id, string newValue)
        {
            GPU gg = this.dbContext.GPUs.First(x => x.GPU_ID == id);
            gg.GPU_NAME = newValue;
            this.dbContext.Set<GPU>().Attach(gg);
            this.dbContext.Entry(gg).State = EntityState.Modified;
            this.dbContext.SaveChanges();
        }

        /// <summary>
        /// Update the price of gpu.
        /// </summary>
        /// <param name="id"> id of gpu. </param>
        /// <param name="newValue"> new price of gpu. </param>
        public void UpdatePrice(int id, int newValue)
        {
            GPU gg = this.dbContext.GPUs.First(x => x.GPU_ID == id);
            gg.PRICE = newValue;
            this.dbContext.Set<GPU>().Attach(gg);
            this.dbContext.Entry(gg).State = EntityState.Modified;
            this.dbContext.SaveChanges();
        }

        /// <summary>
        /// updates whether it is VR ready or not.
        /// </summary>
        /// <param name="id"> id of gpu. </param>
        /// <param name="newValue"> VR ready or not. </param>
        public void UpdateVR(int id, bool newValue)
        {
            GPU gg = this.dbContext.GPUs.First(x => x.GPU_ID == id);
            gg.VR_READY = newValue;
            this.dbContext.Set<GPU>().Attach(gg);
            this.dbContext.Entry(gg).State = EntityState.Modified;
            this.dbContext.SaveChanges();
        }
    }
}
