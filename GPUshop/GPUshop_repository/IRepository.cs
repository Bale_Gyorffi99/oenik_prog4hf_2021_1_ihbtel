﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GPUshop_repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Base interface, containing every CRUD method.
    /// </summary>
    /// <typeparam name="T"> Every table inherits it. </typeparam>
    public interface IRepository<T>
            where T : class
    {
        /// <summary>
        /// Adds an entity to the specified table.
        /// </summary>
        /// <param name="obj"> new entity. </param>
        void CreateItem(T obj);

        /// <summary>
        /// Picks one object from the table. (criteria: ID).
        /// </summary>
        /// <param name="id"> search criteria. </param>
        /// <returns> specified table's object. </returns>
        T GetOne(int id);

        /// <summary>
        /// Lists all entities in a table.
        /// </summary>
        /// <returns> Every entity in a table. </returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Modifies the specified entity's name.
        /// </summary>
        /// <param name="id"> specified entity's ID. </param>
        /// <param name="newValue"> new name. </param>
        void UpdateName(int id, string newValue);

        /// <summary>
        /// Modifies the specified entity's price.
        /// </summary>
        /// <param name="id"> specified entity's ID. </param>
        /// <param name="newValue"> new price. </param>
        void UpdatePrice(int id, int newValue);

        /// <summary>
        /// Deletes the specified entity.
        /// </summary>
        /// <param name="id"> specified ID. </param>
        void DeleteItem(int id);
    }
}
