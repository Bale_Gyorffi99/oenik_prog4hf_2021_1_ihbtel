﻿// <copyright file="Factory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GPUshop_program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GPUshop_data;
    using GPUshop_logic;
    using GPUshop_repository;

    /// <summary>
    /// Factory class.
    /// </summary>
    public class Factory
    {
        private readonly GPULogic gPUlogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="Factory"/> class.
        /// </summary>
        public Factory()
        {
            GPUshopDataBase db = new GPUshopDataBase();

            GPURepository gPURepository = new GPURepository(db);

            GPULogic gPUlogic = new GPULogic(gPURepository);

            this.gPUlogic = gPUlogic;
        }

        /// <summary>
        /// gets gpu logic.
        /// </summary>
        public IGPULogic GetGPULogic => this.gPUlogic;
    }
}
