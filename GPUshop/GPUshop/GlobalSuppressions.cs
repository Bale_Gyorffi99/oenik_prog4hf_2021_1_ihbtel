﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Naming", "CA1707:Identifiers should not contain underscores", Justification = " True ", Scope = "namespace", Target = "~N:GPUshop_program")]
[assembly: SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = " True ", Scope = "member", Target = "~M:GPUshop_program.Factory.#ctor")]
