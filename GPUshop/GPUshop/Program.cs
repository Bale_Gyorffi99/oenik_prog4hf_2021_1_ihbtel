﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GPUshop_program
{
    using System;
    using GPU_Models;
    using GPUshop_data;
    using GPUshop_logic;
    using GPUshop_repository;

    /// <summary>
    /// Main program.
    /// </summary>
    internal class Program
    {
        private static void Main()
        {
            Factory factory = new Factory();

            GetAll(factory.GetGPULogic);
        }

        private static void GetAll(IGPULogic logic)
        {
            System.Linq.IQueryable<GPU> q = logic.GetAll();
            foreach (var item in q)
            {
                Console.WriteLine(item.GPU_NAME);
            }
        }
    }
}
