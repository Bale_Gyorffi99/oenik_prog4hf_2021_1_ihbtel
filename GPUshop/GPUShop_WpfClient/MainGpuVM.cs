﻿// <copyright file="MainGpuVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GPUShop_WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Documents;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using GPUShop_WpfClient;

    /// <summary>
    /// VM of main window.
    /// </summary>
    public class MainGpuVM : ViewModelBase
    {
        private MainGpuLogic logic;
        private ObservableCollection<GpuVM> allGpu;
        private GpuVM selectedGpu;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainGpuVM"/> class.
        /// </summary>
        public MainGpuVM()
        {
            this.logic = new MainGpuLogic(); // TODO: IoC + Dependency Injection !!!

            this.LoadCmd = new RelayCommand(() =>
            this.AllGpu = new ObservableCollection<GpuVM>(this.logic.ApiGetGpus()));
            this.DelCmd = new RelayCommand(() => this.logic.ApiDelGpu(this.selectedGpu));
            this.AddCmd = new RelayCommand(() => this.logic.EditGpu(null, this.EditorFunc));
            this.ModCmd = new RelayCommand(() => this.logic.EditGpu(this.selectedGpu, this.EditorFunc));
        }

        /// <summary>
        /// Gets or sets selected gpu.
        /// </summary>
        public GpuVM SelectedGpu
        {
            get { return this.selectedGpu; }
            set { this.Set(ref this.selectedGpu, value); }
        }

        /// <summary>
        /// Gets or sets every gpu.
        /// </summary>
        public ObservableCollection<GpuVM> AllGpu
        {
            get { return this.allGpu; }
            set { this.Set(ref this.allGpu, value); }
        }

        /// <summary>
        /// Gets or sets what does the editor do.
        /// </summary>
        public Func<GpuVM, bool> EditorFunc { get; set; }

        /// <summary>
        /// Gets add command.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets delete command.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        /// <summary>
        /// Gets modify command.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets load command.
        /// </summary>
        public ICommand LoadCmd { get; private set; }
    }
}
