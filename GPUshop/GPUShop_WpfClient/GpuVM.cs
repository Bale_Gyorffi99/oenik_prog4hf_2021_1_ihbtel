﻿// <copyright file="GpuVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GPUShop_WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// VM of GPU.
    /// </summary>
    public class GpuVM : ObservableObject
    {
        private int gpuId;
        private string gpuName;
        private int price;
        private int ramSize;
        private bool vrReady;
        private int clockSpeed;

        /// <summary>
        /// Gets or sets the ID of GPU.
        /// </summary>
        public int GPUID
        {
            get { return this.gpuId; }
            set { this.Set(ref this.gpuId, value); }
        }

        /// <summary>
        /// Gets or sets name of gpu.
        /// </summary>
        public string GPUNAME
        {
            get { return this.gpuName; }
            set { this.Set(ref this.gpuName, value); }
        }

        /// <summary>
        /// Gets or sets price.
        /// </summary>
        public int PRICE
        {
            get { return this.price; }
            set { this.Set(ref this.price, value); }
        }

        /// <summary>
        /// Gets or sets ram size.
        /// </summary>
        public int RAMSIZE
        {
            get { return this.ramSize; }
            set { this.Set(ref this.ramSize, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether VR is ready.
        /// </summary>
        public bool VRREADY
        {
            get { return this.vrReady; }
            set { this.Set(ref this.vrReady, value); }
        }

        /// <summary>
        /// Gets or sets clock speed.
        /// </summary>
        public int CLOCKSPEED
        {
            get { return this.clockSpeed; }
            set { this.Set(ref this.clockSpeed, value); }
        }

        /// <summary>
        /// creates a copy.
        /// </summary>
        /// <param name="other"> gpu that the copy creates from. </param>
        public void CopyFrom(GpuVM other)
        {
            if (other == null)
            {
                return;
            }

            this.GPUID = other.GPUID;
            this.GPUNAME = other.GPUNAME;
            this.PRICE = other.PRICE;
            this.RAMSIZE = other.RAMSIZE;
        }
    }
}
