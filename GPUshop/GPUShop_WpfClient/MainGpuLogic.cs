﻿// <copyright file="MainGpuLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GPUShop_WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Text.Json;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;
    using GPUshop_logic;

    /// <summary>
    /// Main logic part.
    /// </summary>
    public class MainGpuLogic
    {
        private string url = "http://localhost:35262/GpuApi/";
        private HttpClient client = new HttpClient();
        private JsonSerializerOptions jsonOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);

        /// <summary>
        /// gets all gpu.
        /// </summary>
        /// <returns> every gpu. </returns>
        public List<GpuVM> ApiGetGpus()
        {
            string json = this.client.GetStringAsync(this.url + "all").Result;
            var list = JsonSerializer.Deserialize<List<GpuVM>>(json, this.jsonOptions);

            return list;
        }

        /// <summary>
        /// Deletes a gpu.
        /// </summary>
        /// <param name="gpu"> deletable gpu. </param>
        public void ApiDelGpu(GpuVM gpu)
        {
            bool success = false;
            if (gpu != null)
            {
                string json = this.client.GetStringAsync(this.url + "del/" + gpu.GPUID.ToString()).Result;
                JsonDocument doc = JsonDocument.Parse(json);

                success = doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            }

            SendMessage(success);
        }

        /// <summary>
        /// calls edit gpu method.
        /// </summary>
        /// <param name="gpu"> gpu. </param>
        /// <param name="editor"> an editor. </param>
        public void EditGpu(GpuVM gpu, Func<GpuVM, bool> editor)
        {
            GpuVM clone = new GpuVM();
            if (gpu != null)
            {
                clone.CopyFrom(gpu);
            }

            bool? success = editor?.Invoke(clone);
            if (success == true)
            {
                if (gpu != null)
                {
                    success = this.ApiEditGpu(clone, true);
                }
                else
                {
                    success = this.ApiEditGpu(clone, false);
                }
            }

            SendMessage(success == true);
        }

        /// <summary>
        /// Sends a message.
        /// </summary>
        /// <param name="success"> Success or not. </param>
        private static void SendMessage(bool success)
        {
            string msg = success ? "Operation completed successfully" : "Operation failed";
            Messenger.Default.Send(msg, "GpuResult");
        }

        /// <summary>
        /// API of Edit Gpu.
        /// </summary>
        /// <param name="gpu"> gpu. </param>
        /// <param name="isEditing"> editing?. </param>
        /// <returns> edited or not. </returns>
        private bool ApiEditGpu(GpuVM gpu, bool isEditing)
        {
            if (gpu == null)
            {
                return false;
            }

            string myUrl = isEditing ? this.url + "mod" : this.url + "add";

            if (isEditing == true)
            {
                List<GpuVM> gpuList = this.ApiGetGpus();

                GpuVM modifyGpu = gpuList.SingleOrDefault(x => x.GPUID == gpu.GPUID);
                this.ApiDelGpu(gpuList.SingleOrDefault(x => x.GPUID == gpu.GPUID));

                modifyGpu.GPUID = gpu.GPUID;
                modifyGpu.GPUNAME = gpu.GPUNAME;
                modifyGpu.PRICE = gpu.PRICE;
                modifyGpu.RAMSIZE = gpu.RAMSIZE;
                this.ApiEditGpu(modifyGpu, false);
            }

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing)
            {
                postData.Add("id", gpu.GPUID.ToString());
            }

            postData.Add("gpuName", gpu.GPUNAME);
            postData.Add("ramSize", gpu.RAMSIZE.ToString());
            postData.Add("price", gpu.PRICE.ToString());
            string json = this.client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).
                Result.Content.ReadAsStringAsync().Result;

            JsonDocument doc = JsonDocument.Parse(json);
            return doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
        }
    }
}
