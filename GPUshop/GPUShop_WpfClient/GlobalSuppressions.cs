﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:Elements should appear in the correct order", Justification = "false", Scope = "member", Target = "~M:CarShop.Wpf.MainVM.#ctor")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "false", Scope = "member", Target = "~P:CarShop.Wpf.MainVM.AllCars")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "false", Scope = "member", Target = "~P:GPUShop_WpfClient.MainGpuVM.AllGpu")]
[assembly: SuppressMessage("Design", "CA1001:Types that own disposable fields should be disposable", Justification = "false", Scope = "type", Target = "~T:GPUShop_WpfClient.MainGpuLogic")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "<false>", Scope = "member", Target = "~M:GPUShop_WpfClient.MainGpuLogic.ApiGetGpus~System.Collections.Generic.List{GPUShop_WpfClient.GpuVM}")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "<false>", Scope = "member", Target = "~M:GPUShop_WpfClient.MainGpuLogic.ApiGetGpus~System.Collections.Generic.List{GPUShop_WpfClient.GpuVM}")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<false>", Scope = "member", Target = "~M:GPUShop_WpfClient.MainGpuLogic.ApiDelGpu(GPUShop_WpfClient.GpuVM)")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "<false>", Scope = "member", Target = "~M:GPUShop_WpfClient.MainGpuLogic.ApiDelGpu(GPUShop_WpfClient.GpuVM)")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "<false>", Scope = "member", Target = "~M:GPUShop_WpfClient.MainGpuLogic.ApiEditGpu(GPUShop_WpfClient.GpuVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<false>", Scope = "member", Target = "~M:GPUShop_WpfClient.MainGpuLogic.ApiEditGpu(GPUShop_WpfClient.GpuVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "<false>", Scope = "member", Target = "~M:GPUShop_WpfClient.MainGpuLogic.ApiEditGpu(GPUShop_WpfClient.GpuVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<false>", Scope = "member", Target = "~M:GPUShop_WpfClient.GpuVM.CopyFrom(GPUShop_WpfClient.GpuVM)")]
