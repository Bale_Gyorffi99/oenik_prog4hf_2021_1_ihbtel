﻿// <copyright file="GPUshopDataBase.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GPUshop_data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GPU_Models;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;

    /// <summary>
    /// Class to create database.
    /// </summary>
    public class GPUshopDataBase : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GPUshopDataBase"/> class.
        /// </summary>
        public GPUshopDataBase() => this.Database.EnsureCreated();

        /// <summary>
        /// Gets or sets gPU database.
        /// </summary>
        public DbSet<GPU> GPUs { get; set; }

        /// <summary>
        /// Create method.
        /// </summary>
        /// <param name="modelBuilder"> turtle. </param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            GPU first_gpu = new GPU()
            {
                GPU_ID = 1111,
                SERIALNUMBER = 12345678,
                GPU_NAME = "vidkari_1",
                PRICE = 70000,
                RAM_SIZE = 6,
                CLOCK_SPEED = 1845,
                VR_READY = true,
            };
            GPU second_gpu = new GPU()
            {
                GPU_ID = 1112,
                SERIALNUMBER = 12446618,
                GPU_NAME = "vidkari_2",
                PRICE = 120000,
                RAM_SIZE = 12,
                CLOCK_SPEED = 1945,
                VR_READY = true,
            };
            GPU third_gpu = new GPU()
            {
                GPU_ID = 1113,
                SERIALNUMBER = 22141678,
                GPU_NAME = "vidkari_3",
                PRICE = 50000,
                RAM_SIZE = 4,
                CLOCK_SPEED = 1845,
                VR_READY = false,
            };
            GPU fourth_gpu = new GPU()
            {
                GPU_ID = 1114,
                SERIALNUMBER = 41341628,
                GPU_NAME = "vidkari_4",
                PRICE = 100000,
                RAM_SIZE = 8,
                CLOCK_SPEED = 1745,
                VR_READY = false,
            };
            GPU fifth_gpu = new GPU()
            {
                GPU_ID = 1115,
                SERIALNUMBER = 52341172,
                GPU_NAME = "vidkari_5",
                PRICE = 180000,
                RAM_SIZE = 16,
                CLOCK_SPEED = 2045,
                VR_READY = true,
            };
            modelBuilder.Entity<GPU>()
                .HasData(first_gpu, second_gpu, third_gpu, fourth_gpu, fifth_gpu);
        }

        /// <summary>
        /// Configuring method.
        /// </summary>
        /// <param name="optionsBuilder"> turtle. </param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder
                .UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=
                        |DataDirectory|\gpu_table.mdf;Integrated Security = True");
        }
    }
}
