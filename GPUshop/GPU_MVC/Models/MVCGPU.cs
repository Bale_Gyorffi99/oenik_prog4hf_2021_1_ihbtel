﻿// <copyright file="MVCGPU.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GPU_MVC.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// gpu model of mvc.
    /// </summary>
    public class MVCGPU
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [Display(Name = "GPU ID")]
        [Required]
        public int GpuId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [Display(Name = "GPU NAME")]
        [Required]
        public string GpuName { get; set; }

        /// <summary>
        /// Gets or sets the Ram size.
        /// </summary>
        [Display(Name ="RAM SIZE")]
        [Required]
        public int RamSize { get; set; }

        /// <summary>
        /// Gets or sets the Price.
        /// </summary>
        [Display(Name = "PRICE")]
        [Required]
        public int Price { get; set; }
    }
}
