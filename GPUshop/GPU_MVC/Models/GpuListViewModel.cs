﻿// <copyright file="GpuListViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GPU_MVC.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// view model of gpu list.
    /// </summary>
    public class GpuListViewModel
    {
        /// <summary>
        /// Gets or sets gpu list.
        /// </summary>
        public List<MVCGPU> GpuList { get; set; }

        /// <summary>
        /// Gets or sets gpu.
        /// </summary>
        public MVCGPU EditedGpu { get; set; }
    }
}
