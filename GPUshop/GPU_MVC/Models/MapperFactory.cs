﻿// <copyright file="MapperFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GPU_MVC.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using GPU_Models;

    /// <summary>
    /// Factory of model.
    /// </summary>
    public static class MapperFactory
    {
        /// <summary>
        /// Creates definition of Models.
        /// </summary>
        /// <returns> Imapper thing. </returns>
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<GPU, Models.MVCGPU>().
                ForMember(dest => dest.GpuId, map => map.MapFrom(src => src.GPU_ID)).
                ForMember(dest => dest.GpuName, map => map.MapFrom(src => src.GPU_NAME)).
                ForMember(dest => dest.Price, map => map.MapFrom(src => src.PRICE)).
                ForMember(dest => dest.RamSize, map => map.MapFrom(src => src.RAM_SIZE)).
                ReverseMap();
            });

            return config.CreateMapper();
        }
    }
}
