﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "false", Scope = "member", Target = "~M:GPU_MVC.Controllers.GPUController.#ctor(GPUshop_logic.IGPULogic,AutoMapper.IMapper)")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "false", Scope = "member", Target = "~P:GPU_MVC.Models.GpuListViewModel.GpuList")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "false", Scope = "member", Target = "~P:GPU_MVC.Models.GpuListViewModel.GpuList")]
[assembly: SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "false", Scope = "member", Target = "~M:GPU_MVC.Controllers.GPUController.Remove(System.Int32)~Microsoft.AspNetCore.Mvc.IActionResult")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "false", Scope = "member", Target = "~M:GPU_MVC.Controllers.GpuApiController.ModOneCar(GPU_MVC.Models.MVCGPU)~GPU_MVC.Controllers.GpuApiController.ApiResult")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "false", Scope = "member", Target = "~M:GPU_MVC.Controllers.GpuApiController.ModOneGpu(GPU_MVC.Models.MVCGPU)~GPU_MVC.Controllers.ApiResult")]
