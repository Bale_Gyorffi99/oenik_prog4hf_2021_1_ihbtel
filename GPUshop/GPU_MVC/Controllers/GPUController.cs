﻿// <copyright file="GPUController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GPU_MVC.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using GPU_MVC.Models;
    using GPUshop_logic;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// gpu controller.
    /// </summary>
    public class GPUController : Controller
    {
        private readonly IGPULogic logic;
        private readonly IMapper mapper;
        private readonly GpuListViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="GPUController"/> class.
        /// </summary>
        /// <param name="logic"> logic of GPU.</param>
        /// <param name="mapper"> mapper. </param>
        public GPUController(IGPULogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;

            this.vm = new GpuListViewModel();
            this.vm.EditedGpu = new Models.MVCGPU();

            var gpus = logic.GetAll();
            this.vm.GpuList = mapper.Map<IQueryable<GPU_Models.GPU>, List<Models.MVCGPU>>(gpus);
        }

        /// <summary>
        /// Index.
        /// </summary>
        /// <returns> gpu index and vm. </returns>
        public IActionResult Index()
        {
            this.ViewData["editAction"] = "AddNew";
            return this.View("GPUIndex", this.vm);
        }

        /// <summary>
        /// Gets the looked for gpu.
        /// </summary>
        /// <param name="id"> id of the gpu. </param>
        /// <returns> return the gpu. </returns>
        public Models.MVCGPU GetGpuModel(int id)
        {
            GPU_Models.GPU oneGpu = this.logic.GetOne(id);
            return this.mapper.Map<GPU_Models.GPU, Models.MVCGPU>(oneGpu);
        }

        /// <summary>
        /// Gets the details of the ID gpu.
        /// </summary>
        /// <param name="id"> id of the gpu. </param>
        /// <returns> returns the gpu. </returns>
        public IActionResult Details(int id)
        {
            return this.View("GPUDetails", this.GetGpuModel(id));
        }

        /// <summary>
        /// Removes the looked for gpu.
        /// </summary>
        /// <param name="id"> id of the gpu. </param>
        /// <returns> return the action what happened. </returns>
        public IActionResult Remove(int id)
        {
            this.TempData["editResult"] = "Delete Failed";
            try
            {
                this.logic.DeleteItem(id);
            }
            catch (Exception)
            {
                this.TempData["editResult"] = "Delete Error!";
            }

            if (this.logic.GetOne(id) == null)
            {
                this.TempData["editResult"] = "Delete succeded";
            }

            return this.RedirectToAction(nameof(this.Index));
        }

        /// <summary>
        /// Edits the looked for gpu.
        /// </summary>
        /// <param name="id"> id of the gpu. </param>
        /// <returns> return the edited gpu. </returns>
        public IActionResult Edit(int id)
        {
            this.ViewData["editAction"] = "Edit";
            this.vm.EditedGpu = this.GetGpuModel(id);
            return this.View("GPUIndex", this.vm);
        }

        /// <summary>
        /// Edits or creates new gpu if possible.
        /// </summary>
        /// <param name="gpu"> gpu. </param>
        /// <param name="editAction"> edit actionthing. </param>
        /// <returns> edited or added new or failed. </returns>
        [HttpPost]
        public IActionResult Edit(Models.MVCGPU gpu, string editAction)
        {
            if (this.ModelState.IsValid && gpu != null)
            {
                this.TempData["editResult"] = "Edit succeded";
                if (editAction == "AddNew")
                {
                    this.logic.CreateItem(this.mapper.Map<Models.MVCGPU, GPU_Models.GPU>(gpu));
                }
                else
                {
                    this.logic.UpdateName(gpu.GpuId, gpu.GpuName);
                    this.logic.UpdatePrice(gpu.GpuId, gpu.Price);
                    if (gpu.GpuId != this.logic.GetOne(gpu.GpuId).GPU_ID)
                    {
                        this.TempData["editResult"] = "Edit Failed";
                    }
                }

                return this.RedirectToAction(nameof(this.Index));
            }
            else
            {
                this.ViewData["editAction"] = "Edit";
                this.vm.EditedGpu = gpu;
                return this.View("GPUIndex", this.vm);
            }
        }
    }
}
