﻿// <copyright file="GpuApiController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GPU_MVC.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using GPUshop_logic;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// controller of API.
    /// </summary>
    public class GpuApiController : Controller
    {
        private IGPULogic gpuLogic;

        private IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="GpuApiController"/> class.
        /// </summary>
        /// <param name="gpuLogic"> logic. </param>
        /// <param name="mapper"> mapper. </param>
        public GpuApiController(IGPULogic gpuLogic, IMapper mapper)
        {
            this.gpuLogic = gpuLogic;
            this.mapper = mapper;
        }

        /// <summary>
        /// Gets all gpu.
        /// </summary>
        /// <returns> Every gpu. </returns>
        [ActionName("all")]
        [HttpGet]
        public IEnumerable<Models.MVCGPU> GetAll()
        {
            IQueryable<GPU_Models.GPU> gpus = this.gpuLogic.GetAll();
            List<Models.MVCGPU> q = this.mapper.Map<IQueryable<GPU_Models.GPU>, List<Models.MVCGPU>>(gpus);
            return q;
        }

        /// <summary>
        /// Deletes a car.
        /// </summary>
        /// <param name="id"> id of car. </param>
        /// <returns> apiresult. </returns>
        [ActionName("del")]
        [HttpGet]
        public ApiResult DelOneGpu(int id)
        {
            if (id >= 1111 && id <= 9999)
            {
                this.gpuLogic.DeleteItem(id);
                return new ApiResult() { OperationResult = true };
            }
            else
            {
                return new ApiResult() { OperationResult = false };
            }
        }

        /// <summary>
        /// Adds a new gpu.
        /// </summary>
        /// <param name="gpu"> new gpu. </param>
        /// <returns> succeeded or not. </returns>
        [HttpPost]
        [ActionName("add")]
        public ApiResult AddOneGpu(Models.MVCGPU gpu)
        {
            bool success = true;
            try
            {
                this.gpuLogic.CreateItem(this.mapper.Map<Models.MVCGPU, GPU_Models.GPU>(gpu));
            }
            catch (ArgumentException)
            {
                success = false;
            }

            return new ApiResult() { OperationResult = success };
        }

        /// <summary>
        /// Modifies a gpu.
        /// </summary>
        /// <param name="gpu"> modified gpu. </param>
        /// <returns> succeeded or not. </returns>
        [HttpPost]
        [ActionName("mod")]
        public ApiResult ModOneGpu(Models.MVCGPU gpu)
        {
            if (gpu.GpuId >= 1111 && gpu.GpuId <= 9999)
            {
                this.gpuLogic.UpdateName(gpu.GpuId, gpu.GpuName);
                this.gpuLogic.UpdatePrice(gpu.GpuId, gpu.Price);
                return new ApiResult()
                {
                    OperationResult = true,
                };
            }
            else
            {
                return new ApiResult()
                {
                    OperationResult = false,
                };
            }
        }
    }
}
