var annotated_dup =
[
    [ "GPU_CRUDAPP", "namespace_g_p_u___c_r_u_d_a_p_p.html", [
      [ "BL", "namespace_g_p_u___c_r_u_d_a_p_p_1_1_b_l.html", [
        [ "GPUlogic", "class_g_p_u___c_r_u_d_a_p_p_1_1_b_l_1_1_g_p_ulogic.html", "class_g_p_u___c_r_u_d_a_p_p_1_1_b_l_1_1_g_p_ulogic" ],
        [ "IEditorService", "interface_g_p_u___c_r_u_d_a_p_p_1_1_b_l_1_1_i_editor_service.html", "interface_g_p_u___c_r_u_d_a_p_p_1_1_b_l_1_1_i_editor_service" ],
        [ "IGPUlogic", "interface_g_p_u___c_r_u_d_a_p_p_1_1_b_l_1_1_i_g_p_ulogic.html", "interface_g_p_u___c_r_u_d_a_p_p_1_1_b_l_1_1_i_g_p_ulogic" ]
      ] ],
      [ "Data", "namespace_g_p_u___c_r_u_d_a_p_p_1_1_data.html", [
        [ "GpuModel", "class_g_p_u___c_r_u_d_a_p_p_1_1_data_1_1_gpu_model.html", "class_g_p_u___c_r_u_d_a_p_p_1_1_data_1_1_gpu_model" ]
      ] ],
      [ "UI", "namespace_g_p_u___c_r_u_d_a_p_p_1_1_u_i.html", [
        [ "EditorServiceThroughWindow", "class_g_p_u___c_r_u_d_a_p_p_1_1_u_i_1_1_editor_service_through_window.html", "class_g_p_u___c_r_u_d_a_p_p_1_1_u_i_1_1_editor_service_through_window" ]
      ] ],
      [ "VM", "namespace_g_p_u___c_r_u_d_a_p_p_1_1_v_m.html", [
        [ "EditorViewModel", "class_g_p_u___c_r_u_d_a_p_p_1_1_v_m_1_1_editor_view_model.html", "class_g_p_u___c_r_u_d_a_p_p_1_1_v_m_1_1_editor_view_model" ],
        [ "Factory", "class_g_p_u___c_r_u_d_a_p_p_1_1_v_m_1_1_factory.html", "class_g_p_u___c_r_u_d_a_p_p_1_1_v_m_1_1_factory" ],
        [ "MainViewModel", "class_g_p_u___c_r_u_d_a_p_p_1_1_v_m_1_1_main_view_model.html", "class_g_p_u___c_r_u_d_a_p_p_1_1_v_m_1_1_main_view_model" ]
      ] ],
      [ "App", "class_g_p_u___c_r_u_d_a_p_p_1_1_app.html", "class_g_p_u___c_r_u_d_a_p_p_1_1_app" ],
      [ "EditorWindow", "class_g_p_u___c_r_u_d_a_p_p_1_1_editor_window.html", "class_g_p_u___c_r_u_d_a_p_p_1_1_editor_window" ],
      [ "MainWindow", "class_g_p_u___c_r_u_d_a_p_p_1_1_main_window.html", "class_g_p_u___c_r_u_d_a_p_p_1_1_main_window" ]
    ] ],
    [ "GPUshop_data", "namespace_g_p_ushop__data.html", [
      [ "GPUshopDataBase", "class_g_p_ushop__data_1_1_g_p_ushop_data_base.html", "class_g_p_ushop__data_1_1_g_p_ushop_data_base" ]
    ] ],
    [ "GPUshop_logic", "namespace_g_p_ushop__logic.html", [
      [ "GPULogic", "class_g_p_ushop__logic_1_1_g_p_u_logic.html", "class_g_p_ushop__logic_1_1_g_p_u_logic" ],
      [ "IGPULogic", "interface_g_p_ushop__logic_1_1_i_g_p_u_logic.html", "interface_g_p_ushop__logic_1_1_i_g_p_u_logic" ],
      [ "ILogic", "interface_g_p_ushop__logic_1_1_i_logic.html", "interface_g_p_ushop__logic_1_1_i_logic" ]
    ] ],
    [ "GPUshop_program", "namespace_g_p_ushop__program.html", [
      [ "Factory", "class_g_p_ushop__program_1_1_factory.html", "class_g_p_ushop__program_1_1_factory" ],
      [ "Program", "class_g_p_ushop__program_1_1_program.html", null ]
    ] ],
    [ "GPUshop_repository", "namespace_g_p_ushop__repository.html", [
      [ "GPURepository", "class_g_p_ushop__repository_1_1_g_p_u_repository.html", "class_g_p_ushop__repository_1_1_g_p_u_repository" ],
      [ "IGPURepository", "interface_g_p_ushop__repository_1_1_i_g_p_u_repository.html", "interface_g_p_ushop__repository_1_1_i_g_p_u_repository" ],
      [ "IRepository", "interface_g_p_ushop__repository_1_1_i_repository.html", "interface_g_p_ushop__repository_1_1_i_repository" ]
    ] ],
    [ "Models", "namespace_models.html", [
      [ "GPU", "class_models_1_1_g_p_u.html", "class_models_1_1_g_p_u" ]
    ] ],
    [ "XamlGeneratedNamespace", "namespace_xaml_generated_namespace.html", [
      [ "GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", "class_xaml_generated_namespace_1_1_generated_internal_type_helper" ]
    ] ]
];