var class_g_p_ushop__logic_1_1_g_p_u_logic =
[
    [ "GPULogic", "class_g_p_ushop__logic_1_1_g_p_u_logic.html#a651fe09e05a3f61eb7fcff39be505942", null ],
    [ "AVGPrice", "class_g_p_ushop__logic_1_1_g_p_u_logic.html#a5765e4acdee261169262a179c770f964", null ],
    [ "CreateItem", "class_g_p_ushop__logic_1_1_g_p_u_logic.html#ade3b953675e638e0b600757cc50647e2", null ],
    [ "DeleteItem", "class_g_p_ushop__logic_1_1_g_p_u_logic.html#a0719e7ef08fd1da7aeaa4f94b076e72d", null ],
    [ "FilterPriceBetween", "class_g_p_ushop__logic_1_1_g_p_u_logic.html#a55161aada59fb049676ab98e23099139", null ],
    [ "GetAll", "class_g_p_ushop__logic_1_1_g_p_u_logic.html#a924969d82b16b2ae6be66b7852e21454", null ],
    [ "GetMaxIdx", "class_g_p_ushop__logic_1_1_g_p_u_logic.html#a21019f04dde44b8fad706b5203e65a37", null ],
    [ "GetOne", "class_g_p_ushop__logic_1_1_g_p_u_logic.html#aec477bd07f35b1832b33087e46a02ff0", null ],
    [ "UpdateName", "class_g_p_ushop__logic_1_1_g_p_u_logic.html#a68965c5688f432e49c6d41f0530ea772", null ],
    [ "UpdatePrice", "class_g_p_ushop__logic_1_1_g_p_u_logic.html#a29ebadd9e6d5b1b7c28c03e90acd4e7e", null ],
    [ "UpdateVR", "class_g_p_ushop__logic_1_1_g_p_u_logic.html#a94e3fdfa5c49026653aa3675b46c89c8", null ]
];