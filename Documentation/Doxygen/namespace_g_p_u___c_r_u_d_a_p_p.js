var namespace_g_p_u___c_r_u_d_a_p_p =
[
    [ "BL", "namespace_g_p_u___c_r_u_d_a_p_p_1_1_b_l.html", "namespace_g_p_u___c_r_u_d_a_p_p_1_1_b_l" ],
    [ "Data", "namespace_g_p_u___c_r_u_d_a_p_p_1_1_data.html", "namespace_g_p_u___c_r_u_d_a_p_p_1_1_data" ],
    [ "UI", "namespace_g_p_u___c_r_u_d_a_p_p_1_1_u_i.html", "namespace_g_p_u___c_r_u_d_a_p_p_1_1_u_i" ],
    [ "VM", "namespace_g_p_u___c_r_u_d_a_p_p_1_1_v_m.html", "namespace_g_p_u___c_r_u_d_a_p_p_1_1_v_m" ],
    [ "App", "class_g_p_u___c_r_u_d_a_p_p_1_1_app.html", "class_g_p_u___c_r_u_d_a_p_p_1_1_app" ],
    [ "EditorWindow", "class_g_p_u___c_r_u_d_a_p_p_1_1_editor_window.html", "class_g_p_u___c_r_u_d_a_p_p_1_1_editor_window" ],
    [ "MainWindow", "class_g_p_u___c_r_u_d_a_p_p_1_1_main_window.html", "class_g_p_u___c_r_u_d_a_p_p_1_1_main_window" ]
];