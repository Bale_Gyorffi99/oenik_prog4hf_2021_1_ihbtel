var searchData=
[
  ['clock_5fspeed_6',['CLOCK_SPEED',['../class_g_p_u___c_r_u_d_a_p_p_1_1_data_1_1_gpu_model.html#a3e2f97350ef3c7340c2cc94527522cdb',1,'GPU_CRUDAPP.Data.GpuModel.CLOCK_SPEED()'],['../class_models_1_1_g_p_u.html#aff58a8033636f965abf4b18f7050831a',1,'Models.GPU.CLOCK_SPEED()']]],
  ['copyfrom_7',['CopyFrom',['../class_g_p_u___c_r_u_d_a_p_p_1_1_data_1_1_gpu_model.html#af655d83e9d853cd7e9d970115852b7a7',1,'GPU_CRUDAPP.Data.GpuModel.CopyFrom()'],['../class_models_1_1_g_p_u.html#a72661d02b053d343c1103011e6a7cc17',1,'Models.GPU.CopyFrom()']]],
  ['createdelegate_8',['CreateDelegate',['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#a8ec4c37e82d9f4e867e9655f4eac3a78',1,'XamlGeneratedNamespace::GeneratedInternalTypeHelper']]],
  ['createinstance_9',['CreateInstance',['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#aefb7a98fceb9c287cef4756942f441d1',1,'XamlGeneratedNamespace::GeneratedInternalTypeHelper']]],
  ['createitem_10',['CreateItem',['../class_g_p_ushop__logic_1_1_g_p_u_logic.html#ade3b953675e638e0b600757cc50647e2',1,'GPUshop_logic.GPULogic.CreateItem()'],['../interface_g_p_ushop__logic_1_1_i_logic.html#a0f4883f9f34a03ff5ca2f025e01f3137',1,'GPUshop_logic.ILogic.CreateItem()'],['../class_g_p_ushop__repository_1_1_g_p_u_repository.html#a5cf389fdc414a4f0db28e3fcb1ca366b',1,'GPUshop_repository.GPURepository.CreateItem()'],['../interface_g_p_ushop__repository_1_1_i_repository.html#af04c061791add6b280695a8bf55bbacf',1,'GPUshop_repository.IRepository.CreateItem()']]]
];
