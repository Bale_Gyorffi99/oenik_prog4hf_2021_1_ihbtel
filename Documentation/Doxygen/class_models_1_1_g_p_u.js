var class_models_1_1_g_p_u =
[
    [ "CopyFrom", "class_models_1_1_g_p_u.html#a72661d02b053d343c1103011e6a7cc17", null ],
    [ "CLOCK_SPEED", "class_models_1_1_g_p_u.html#aff58a8033636f965abf4b18f7050831a", null ],
    [ "GPU_ID", "class_models_1_1_g_p_u.html#a85dde825eca719b19c61bdb4bff2232f", null ],
    [ "GPU_NAME", "class_models_1_1_g_p_u.html#aad28286524e65e440ec2230d278252a6", null ],
    [ "PRICE", "class_models_1_1_g_p_u.html#a0bba1b615040274579f44c99e11133fa", null ],
    [ "RAM_SIZE", "class_models_1_1_g_p_u.html#a6871414f0618300d9d0d48307b364924", null ],
    [ "SERIALNUMBER", "class_models_1_1_g_p_u.html#a451d44fc1cd05c4c9822cd8fdee26cb0", null ],
    [ "VR_READY", "class_models_1_1_g_p_u.html#a3820a8110fef54b3c27110e97b9583bf", null ]
];