var class_g_p_u___c_r_u_d_a_p_p_1_1_data_1_1_gpu_model =
[
    [ "CopyFrom", "class_g_p_u___c_r_u_d_a_p_p_1_1_data_1_1_gpu_model.html#af655d83e9d853cd7e9d970115852b7a7", null ],
    [ "CLOCK_SPEED", "class_g_p_u___c_r_u_d_a_p_p_1_1_data_1_1_gpu_model.html#a3e2f97350ef3c7340c2cc94527522cdb", null ],
    [ "GPU_ID", "class_g_p_u___c_r_u_d_a_p_p_1_1_data_1_1_gpu_model.html#a264636ea8ec1a6d31e31b21759f64ebd", null ],
    [ "GPU_NAME", "class_g_p_u___c_r_u_d_a_p_p_1_1_data_1_1_gpu_model.html#ab52ef780b57fe62f4434e909f2f52bb2", null ],
    [ "PRICE", "class_g_p_u___c_r_u_d_a_p_p_1_1_data_1_1_gpu_model.html#a332989a71a4ca00200b1cbc4da971104", null ],
    [ "RAM_SIZE", "class_g_p_u___c_r_u_d_a_p_p_1_1_data_1_1_gpu_model.html#a3793df35f8fdefd1b2a09a4dc6fd0d76", null ],
    [ "VR_READY", "class_g_p_u___c_r_u_d_a_p_p_1_1_data_1_1_gpu_model.html#a0fa1db0acb19c48fac176425b09c9e6e", null ]
];