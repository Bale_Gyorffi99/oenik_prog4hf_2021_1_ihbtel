var namespaces_dup =
[
    [ "GPU_CRUDAPP", "namespace_g_p_u___c_r_u_d_a_p_p.html", "namespace_g_p_u___c_r_u_d_a_p_p" ],
    [ "GPUshop_data", "namespace_g_p_ushop__data.html", "namespace_g_p_ushop__data" ],
    [ "GPUshop_logic", "namespace_g_p_ushop__logic.html", "namespace_g_p_ushop__logic" ],
    [ "GPUshop_program", "namespace_g_p_ushop__program.html", "namespace_g_p_ushop__program" ],
    [ "GPUshop_repository", "namespace_g_p_ushop__repository.html", "namespace_g_p_ushop__repository" ],
    [ "Models", "namespace_models.html", "namespace_models" ],
    [ "XamlGeneratedNamespace", "namespace_xaml_generated_namespace.html", "namespace_xaml_generated_namespace" ]
];