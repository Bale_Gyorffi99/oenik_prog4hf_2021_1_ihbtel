var interface_g_p_ushop__repository_1_1_i_repository =
[
    [ "CreateItem", "interface_g_p_ushop__repository_1_1_i_repository.html#af04c061791add6b280695a8bf55bbacf", null ],
    [ "DeleteItem", "interface_g_p_ushop__repository_1_1_i_repository.html#aae2573d882097c93ce87b0f9d9713a48", null ],
    [ "GetAll", "interface_g_p_ushop__repository_1_1_i_repository.html#aea8b4c2a51365cfd1ec835028e536c8d", null ],
    [ "GetOne", "interface_g_p_ushop__repository_1_1_i_repository.html#aa7fae26a7889bb86e716400598023c53", null ],
    [ "UpdateName", "interface_g_p_ushop__repository_1_1_i_repository.html#a773ad1ba3a3eb70e1db67846f6052f35", null ],
    [ "UpdatePrice", "interface_g_p_ushop__repository_1_1_i_repository.html#ac189029adeee172693ae12749aaebccc", null ]
];