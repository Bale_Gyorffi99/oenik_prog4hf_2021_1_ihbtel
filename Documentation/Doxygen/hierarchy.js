var hierarchy =
[
    [ "Application", null, [
      [ "GPU_CRUDAPP.App", "class_g_p_u___c_r_u_d_a_p_p_1_1_app.html", null ]
    ] ],
    [ "System.Windows.Application", null, [
      [ "GPU_CRUDAPP.App", "class_g_p_u___c_r_u_d_a_p_p_1_1_app.html", null ],
      [ "GPU_CRUDAPP.App", "class_g_p_u___c_r_u_d_a_p_p_1_1_app.html", null ],
      [ "GPU_CRUDAPP.App", "class_g_p_u___c_r_u_d_a_p_p_1_1_app.html", null ],
      [ "GPU_CRUDAPP.App", "class_g_p_u___c_r_u_d_a_p_p_1_1_app.html", null ]
    ] ],
    [ "DbContext", null, [
      [ "GPUshop_data.GPUshopDataBase", "class_g_p_ushop__data_1_1_g_p_ushop_data_base.html", null ]
    ] ],
    [ "GPU_CRUDAPP.VM.Factory", "class_g_p_u___c_r_u_d_a_p_p_1_1_v_m_1_1_factory.html", null ],
    [ "GPUshop_program.Factory", "class_g_p_ushop__program_1_1_factory.html", null ],
    [ "System.Windows.Markup.IComponentConnector", null, [
      [ "GPU_CRUDAPP.EditorWindow", "class_g_p_u___c_r_u_d_a_p_p_1_1_editor_window.html", null ],
      [ "GPU_CRUDAPP.EditorWindow", "class_g_p_u___c_r_u_d_a_p_p_1_1_editor_window.html", null ],
      [ "GPU_CRUDAPP.MainWindow", "class_g_p_u___c_r_u_d_a_p_p_1_1_main_window.html", null ],
      [ "GPU_CRUDAPP.MainWindow", "class_g_p_u___c_r_u_d_a_p_p_1_1_main_window.html", null ],
      [ "GPU_CRUDAPP.MainWindow", "class_g_p_u___c_r_u_d_a_p_p_1_1_main_window.html", null ],
      [ "GPU_CRUDAPP.MainWindow", "class_g_p_u___c_r_u_d_a_p_p_1_1_main_window.html", null ]
    ] ],
    [ "GPU_CRUDAPP.BL.IEditorService", "interface_g_p_u___c_r_u_d_a_p_p_1_1_b_l_1_1_i_editor_service.html", [
      [ "GPU_CRUDAPP.UI.EditorServiceThroughWindow", "class_g_p_u___c_r_u_d_a_p_p_1_1_u_i_1_1_editor_service_through_window.html", null ]
    ] ],
    [ "GPU_CRUDAPP.BL.IGPUlogic", "interface_g_p_u___c_r_u_d_a_p_p_1_1_b_l_1_1_i_g_p_ulogic.html", [
      [ "GPU_CRUDAPP.BL.GPUlogic", "class_g_p_u___c_r_u_d_a_p_p_1_1_b_l_1_1_g_p_ulogic.html", null ]
    ] ],
    [ "GPUshop_logic.ILogic< T >", "interface_g_p_ushop__logic_1_1_i_logic.html", null ],
    [ "GPUshop_logic.ILogic< GPU >", "interface_g_p_ushop__logic_1_1_i_logic.html", [
      [ "GPUshop_logic.IGPULogic", "interface_g_p_ushop__logic_1_1_i_g_p_u_logic.html", [
        [ "GPUshop_logic.GPULogic", "class_g_p_ushop__logic_1_1_g_p_u_logic.html", null ]
      ] ]
    ] ],
    [ "System.Windows.Markup.InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "GPUshop_repository.IRepository< T >", "interface_g_p_ushop__repository_1_1_i_repository.html", null ],
    [ "GPUshop_repository.IRepository< GPU >", "interface_g_p_ushop__repository_1_1_i_repository.html", [
      [ "GPUshop_repository.IGPURepository", "interface_g_p_ushop__repository_1_1_i_g_p_u_repository.html", [
        [ "GPUshop_repository.GPURepository", "class_g_p_ushop__repository_1_1_g_p_u_repository.html", null ]
      ] ]
    ] ],
    [ "ObservableObject", null, [
      [ "GPU_CRUDAPP.Data.GpuModel", "class_g_p_u___c_r_u_d_a_p_p_1_1_data_1_1_gpu_model.html", null ],
      [ "Models.GPU", "class_models_1_1_g_p_u.html", null ]
    ] ],
    [ "GPUshop_program.Program", "class_g_p_ushop__program_1_1_program.html", null ],
    [ "ViewModelBase", null, [
      [ "GPU_CRUDAPP.VM.EditorViewModel", "class_g_p_u___c_r_u_d_a_p_p_1_1_v_m_1_1_editor_view_model.html", null ],
      [ "GPU_CRUDAPP.VM.MainViewModel", "class_g_p_u___c_r_u_d_a_p_p_1_1_v_m_1_1_main_view_model.html", null ]
    ] ],
    [ "System.Windows.Window", null, [
      [ "GPU_CRUDAPP.EditorWindow", "class_g_p_u___c_r_u_d_a_p_p_1_1_editor_window.html", null ],
      [ "GPU_CRUDAPP.EditorWindow", "class_g_p_u___c_r_u_d_a_p_p_1_1_editor_window.html", null ],
      [ "GPU_CRUDAPP.MainWindow", "class_g_p_u___c_r_u_d_a_p_p_1_1_main_window.html", null ],
      [ "GPU_CRUDAPP.MainWindow", "class_g_p_u___c_r_u_d_a_p_p_1_1_main_window.html", null ],
      [ "GPU_CRUDAPP.MainWindow", "class_g_p_u___c_r_u_d_a_p_p_1_1_main_window.html", null ],
      [ "GPU_CRUDAPP.MainWindow", "class_g_p_u___c_r_u_d_a_p_p_1_1_main_window.html", null ],
      [ "GPU_CRUDAPP.MainWindow", "class_g_p_u___c_r_u_d_a_p_p_1_1_main_window.html", null ]
    ] ],
    [ "Window", null, [
      [ "GPU_CRUDAPP.EditorWindow", "class_g_p_u___c_r_u_d_a_p_p_1_1_editor_window.html", null ]
    ] ]
];