var interface_g_p_ushop__logic_1_1_i_logic =
[
    [ "CreateItem", "interface_g_p_ushop__logic_1_1_i_logic.html#a0f4883f9f34a03ff5ca2f025e01f3137", null ],
    [ "DeleteItem", "interface_g_p_ushop__logic_1_1_i_logic.html#a214914957d4b1d8e23924d67e5e21150", null ],
    [ "GetAll", "interface_g_p_ushop__logic_1_1_i_logic.html#a0d551a8d842930de02e3f7fd9b18686b", null ],
    [ "GetMaxIdx", "interface_g_p_ushop__logic_1_1_i_logic.html#a741c98bd2e7d6b1b07b82f106de1c2aa", null ],
    [ "GetOne", "interface_g_p_ushop__logic_1_1_i_logic.html#aee02d4be8ce7e3a7fe414b40f25496bb", null ],
    [ "UpdateName", "interface_g_p_ushop__logic_1_1_i_logic.html#ab836b4ac4b4b4fc71c104697ace29413", null ],
    [ "UpdatePrice", "interface_g_p_ushop__logic_1_1_i_logic.html#a6a9a7a87d95c026151ee0e306e78c08f", null ]
];