var class_g_p_ushop__repository_1_1_g_p_u_repository =
[
    [ "GPURepository", "class_g_p_ushop__repository_1_1_g_p_u_repository.html#ae72dcd7a2dddc0466788fc6586db5384", null ],
    [ "CreateItem", "class_g_p_ushop__repository_1_1_g_p_u_repository.html#a5cf389fdc414a4f0db28e3fcb1ca366b", null ],
    [ "DeleteItem", "class_g_p_ushop__repository_1_1_g_p_u_repository.html#a39516a545035c2b96ffbda3a86620b9d", null ],
    [ "GetAll", "class_g_p_ushop__repository_1_1_g_p_u_repository.html#af31131e2f774dba1f5000b82334e870f", null ],
    [ "GetOne", "class_g_p_ushop__repository_1_1_g_p_u_repository.html#a81c1dc6488022ea9f0ab125771e5ddc0", null ],
    [ "UpdateName", "class_g_p_ushop__repository_1_1_g_p_u_repository.html#a143555714168efb15ac31acf8f41868e", null ],
    [ "UpdatePrice", "class_g_p_ushop__repository_1_1_g_p_u_repository.html#a57188cbfc85f22a128ded3a44dee0d2a", null ],
    [ "UpdateVR", "class_g_p_ushop__repository_1_1_g_p_u_repository.html#a0885b8dc1d2850b4f512a4536e93f12d", null ]
];